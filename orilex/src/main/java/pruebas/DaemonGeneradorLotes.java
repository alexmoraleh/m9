package pruebas;

import java.util.ArrayList;
import java.util.Random;

import dao.LicitacionesDao;
import dao.LoteDao;
import mapping.Licitaciones;
import mapping.Lote;

public class DaemonGeneradorLotes extends Thread {
	
	static Random rnd = new Random();
	
	public void run() {
		LoteDao ld = new LoteDao();
		while(true) {
			ld.saveOrUpdate(getLote());
			try {
				sleep(100000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public Lote getLote() {
		LicitacionesDao ldd = new LicitacionesDao();
		Licitaciones lll = ldd.get(1);
		ArrayList<Lote> lotes = new ArrayList<Lote>();
		Lote lo = new Lote(1, rnd.nextInt(7000), "Pastel de Pere", lll);
		lotes.add(lo);
		Lote la = new Lote(2, rnd.nextInt(7000), "Diabolo Shiny", lll);
		lotes.add(la);
		Lote le = new Lote(3, rnd.nextInt(7000), "Castillo de Chocolate", lll);
		lotes.add(le);
		Lote lu = new Lote(4, rnd.nextInt(7000), "Bombardeo Azul", lll);
		lotes.add(lu);
		Lote li = new Lote(5, rnd.nextInt(7000), "Magia Pastelada", lll);
		lotes.add(li);
		Lote lz = new Lote(6, rnd.nextInt(7000), "Destructor Licorial", lll);
		lotes.add(lz);
		Lote lv = new Lote(7, rnd.nextInt(7000), "Pastel de 12 capas", lll);
		lotes.add(lv);
		Lote lm = new Lote(8, rnd.nextInt(7000), "Rey pastel", lll);
		lotes.add(lm);
		Lote ln = new Lote(9, rnd.nextInt(7000), "TastatAzul Explosivo", lll);
		lotes.add(ln);
		Lote ly = new Lote(10, rnd.nextInt(7000), "Pastel a la Thread", lll);
		lotes.add(ly);
		
		return lotes.get(rnd.nextInt(9));
	}
}
