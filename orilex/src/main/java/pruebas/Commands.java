package pruebas;

import mapping.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Semaphore;

import dao.*;

public class Commands extends Thread {
	private Socket client;
	public static volatile String msgs;
	private String oldmsgs = new String();
	public static volatile ArrayList<String> commandas = new ArrayList<String>();
	public static volatile ArrayList<String> entradas = new ArrayList<String>();
	public static volatile Map<String, Integer> index = new HashMap<String, Integer>();
	public volatile Map<String, ArrayList<String>> clientes = new HashMap<String, ArrayList<String>>();
	private boolean start = true;
	public static volatile Semaphore eready;
	public static volatile Semaphore EnEspera = new Semaphore(1);
	public volatile String user = "";
	private Cliente current = new Cliente();
	private Random rnd = new Random();
	private PrintWriter txStream;
	private BufferedReader rxStream;
	public volatile boolean substart;
	private ServerMsgs sender;
	private ClienteMsgs buzon;

	public Commands(Socket s, String msgs, ArrayList<String> commandas, Map<String, Integer> index, boolean substart, ArrayList<String> entradas, Map<String, ArrayList<String>> clientes)
			throws IOException {
		this.client = s;
		this.msgs = msgs;
		this.commandas = commandas;
		this.index = index;
		this.txStream = new PrintWriter(s.getOutputStream(), true);
		this.rxStream = new BufferedReader(new InputStreamReader(s.getInputStream()));
		this.substart = substart;
		this.entradas = entradas;
		this.sender = new ServerMsgs(msgs, s, EnEspera);
		this.clientes = clientes;
	}

	@Override
	public void run() {
		oldmsgs = msgs;
		this.sender.start();
		String res = "";
		while (true) {
			try {
				if (start) {
					res = "ASSISTENTE: Bienvenido a Testado Azul. /help para mas informacion sobre los comandos disponibles.";
					res += respuesta("/help", true);
					txStream.println(res);
					start = false;
					res = "";
				}

				String rx = rxStream.readLine();
				if (rx.equals("/exit")) {
					client.close();
					txStream.println("ASSISTENTE: Te has desconectado.");
					this.sender.interrupt();
					break;
				}

				if (commandas.contains(rx.split(" ")[0])) {
					res = respuesta(rx, true);
				} else {
					res = respuesta(rx, false);
				}
				txStream.println(res);
			} catch (Exception e) {
				System.err.println("ASSISTENTE: ERROR??.");
				e.printStackTrace();
				this.sender.interrupt();
				if(buzon.isAlive()) {
					this.buzon.interrupt();
				}
				break;
			}

		}

	}

	public String respuesta(String rx, boolean check) throws IOException, InterruptedException {
		if (check) {
			int num = index.get(rx.split(" ")[0]);
			switch (num) {
			case 0:
				rx = help();
				break;
			case 1:
				String[] split = rx.split(" ");
				if(split.length == 2) {
					String[] userpass = split[1].split(":");
					if(userpass.length == 2) {
						Cliente usuario = check(userpass[0], userpass[1]);
						if (usuario != null) {
							user = usuario.getUsuario();
							current = usuario;
							rx = "ASSISTENTE: Login correcto. \nBienvenido "+ user +".";
							rx += "Saldo actual: "+ current.getSactual();
							EnEspera.acquire();
							clientes.put(user, new ArrayList<String>());
							EnEspera.release();
							this.buzon = new ClienteMsgs(user, clientes, client);
							this.buzon.start();
						} else {
							return "ASSISTENTE: Usuario o contraseña incorrectos.";
						}
					}
				}
				
				break;
			case 2:
				String[] register = rx.split(" ");
				if(register.length == 2) {
					String[] creating = register[1].split(":");
					if(creating.length == 3) {
						if (creating[1].equals(creating[2]) && creating[0] != "SUBASTA") {
							if (regis(creating[0], creating[1])) {
								rx = "ASSISTENTE: Usuario creado, use /login para logearse";
							} else
								rx = "ASSISTENTE: El usuario ya existe.";
						} else {
							rx = "ASSISTENTE: Las contraseñas no son iguales";
						}
					} else {
						rx = "ASSISTENTE: Formato erroneo.";
					}
				} else {
					rx = "ASSISTENTE: Comanda introducida erronea.";
				}
				
				break;
			case 3:
				if(!user.isEmpty()) {
					substart = Server.substartlot;
					String money = rx.split(" ")[1];
					current = new ClienteDao().get(current.getId());
					if(current.getSactual() >= Integer.parseInt(money) && substart) {
						Lote le = Server.le;
						if(le.getPrecioadjudicado() != 0) {
							if(le.getPrecioadjudicado() < Integer.parseInt(money)) {
								entradas.add(user+":"+money);
								rx = "ASSISTENTE: Has pujado con exito!";
							} else {
								rx = "Hay una puja mas alta que la tuya!";
							}
						} else {
							if(Integer.parseInt(money) > le.getPrecio()) {
								entradas.add(user+":"+money);
								rx = "ASSISTENTE: Has pujado con exito!";
							} else {
								rx = "ASSISTENTE: La puja inicial es mas alta!";
							}
						}
					} else {
						if(!substart) {
							rx = "ASSISTENTE: No hay ninguna subasta activa. " + substart;
						} else {
							rx = "ASSISTENTE: No tienes tanto dinero. Dinero actual: "+ current.getSactual();
						}
					}
				} else {
					rx = "ASSISTENTE: No estas logeado";
				}
				break;
			case 4:
				if (!user.isEmpty()) {
					clientes.remove(user);
					user = "";
					rx = "ASSISTENTE: Log out exitoso!";
				} else {
					rx = "ASSISTENTE: No estas logeado";
				}

			break;
			case 5:
				if(Server.substartlot) {
					Lote le = Server.le;
					if(le.getPrecioadjudicado() == 0) {
						rx = "ASSISTENTE: Nombre del lote actual: "+ le.getNombre() + ", precio actual del lote: "+ le.getPrecio();
					} else {
						rx = "ASSISTENTE: Nombre del lote actual: "+ le.getNombre() + ", precio actual del lote: "+ le.getPrecioadjudicado();
					}
					rx += "\nTu saldo actual: "+ current.getSactual();
				} else {
					rx = "ASSISTENTE: No hay ninguna licitación activo";
				}
			break;
			case 6:
				if(sender.isMsgson()) {
					sender.setMsgson(false);
					rx = "ASSISTENTE: El servidor no te avisara de los estados de la subasta.";
				} else {
					sender.setMsgson(true);
					rx = "ASSISTENTE: El servidor te avisara de los estados de la subasta.";
				}
				
			break;
			
			case 7:
				if(!user.isEmpty()) {
					String[] mssg = rx.split(":");
					if(mssg.length == 2) {
						String[] reciver = mssg[0].split(" ");
							if(reciver.length == 2) {
								ArrayList<String> mssgreciver = clientes.get(reciver[1]);
								if(mssgreciver != null) {
									System.out.println(mssg[1]);
									clientes.get(reciver[1]).add(user+": "+mssg[1]);
									rx = "";
							} else {
								rx = "ASSISTENTE: Algo esta mal";
							}
						} else {
							rx = "ASSISTENTE: No hay ningun usuario existente que se llame asi.";
						}
					} else {
						rx = "ASSISTENTE: Formato incorrecto.";
					}
				} else {
					rx = "ASSISTENTE: No estas logeado";
				}
			break;
			}
			return rx;
		} else {
			return "ASSISTENTE: Comanda no disponible.";
		}
	}

	public String help() {
		
		String msg = "====================================================================================================================================="
				+ "\n====================================================================================================================================="
				+ "\n====================================================================================================================================="
				+ "\nASSISTENTE: Comandos disponibles:" + "\n/help --> Comandos disponibles" + "\n/login --> user:password. Te logeas en la subasta actual."
				+ "\n/register --> user:password:password.Te registra como cliente."
				+ "\n/push --> cantidad. Hace una oferta a la subasta actual."
				+ "\n/logout --> Borra el usuario actual."
				+ "\n/get --> Devuelve el lote actual si existe."
				+ "\n/turn --> ACTIVA(on)/DESACTIVA(off) las notificaciones del servidor."
				+ "\n/exit --> te desconectas del servidor."
				+ "\n/msg --> user:mensaje. Envias un mensaje al usuario que quieres, si este esta logeado claro." 
				+ "====================================================================================================================================="
				+ "\n====================================================================================================================================="
				+ "\n=====================================================================================================================================";
		return msg;
	}

	public Cliente check(String user, String password) {
		Cliente cli = null;
		List<Cliente> lasdjfkljf = new ClienteDao().list();
		for (Cliente c : lasdjfkljf) {
			if (c.login(user, password)) {
				cli = c;
				break;
			}
		}
		return cli;
	}

	public boolean regis(String user, String password) {
		Cliente cli = new Cliente(user, password, rnd.nextInt(50000));
		new ClienteDao().saveOrUpdate(cli);
		return true;
	}

}
