package pruebas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.Semaphore;

public class ServerMsgs extends Thread{
	
	private String msgs;
	private String oldMsgs;
	private PrintWriter txStream;
	public volatile boolean msgson = true;
	protected Semaphore EnEspera;
	
	public ServerMsgs(String msgs, Socket Client, Semaphore EnEspera) throws IOException {
		this.msgs = msgs;
		this.txStream = new PrintWriter(Client.getOutputStream(), true);
		this.EnEspera = EnEspera;
	}
	
	
	public void run() {
		oldMsgs = "";
		while(true) {
			msgs = Server.msgs;
			if (!msgs.equals(oldMsgs) && msgson) {
				txStream.println(msgs);
				oldMsgs = msgs;
			}
		}
	}


	public boolean isMsgson() {
		return msgson;
	}


	public void setMsgson(boolean msgson) {
		this.msgson = msgson;
	}
	
	
}
