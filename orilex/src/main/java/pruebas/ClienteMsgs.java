package pruebas;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;

public class ClienteMsgs extends Thread {

	public static volatile Map<String, ArrayList<String>> clientes;
	public volatile String user = "";
	private PrintWriter txStream;
	private int current = 0;

	public ClienteMsgs(String user, Map<String, ArrayList<String>> clientes, Socket s) throws IOException {
		this.user = user;
		this.clientes = clientes;
		this.txStream = new PrintWriter(s.getOutputStream(), true);
	}

	public void run() {
		while (true) {
			if (!clientes.get(user).isEmpty()) {
				if(clientes.get(user).size() > current) {
					String mssgs = clientes.get(user).get(current);
					if (!mssgs.isEmpty()) {
						txStream.println(mssgs);
						current++;
					}
				}
			}
		}
	}
}
