package pruebas;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.channels.NonWritableChannelException;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.Semaphore;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import dao.ClienteDao;
import dao.LicitacionesDao;
import dao.LoteDao;
import mapping.Cliente;
import mapping.Licitaciones;
import mapping.Lote;

public class Server extends Thread{
	static Scanner reader = new Scanner(System.in);
	private static ServerSocket servidor;
	public static volatile String msgs = new String();
	public static volatile ArrayList<String> entradas = new ArrayList<String>();
	public static volatile Map<String, ArrayList<String>> clientes = new HashMap<String, ArrayList<String>>();
	private static int current = 0;
	private static long start = 0;
	private static long end_sub = 0;
	public static volatile ArrayList<String> commandas = new ArrayList<String>();
	public static volatile Map<String, Integer> index = new HashMap<String, Integer>();
	public static boolean substart=true;
	public volatile static boolean substartlot=false;
	private static ArrayList<Lote> lotes_sub = new ArrayList<Lote>();
	private static long last_time;
	private static long bid_time;
	public static Lote le;
	
	public static Semaphore read = new Semaphore(1);
	
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;
	public static synchronized SessionFactory getSessionFactory() {
	    if ( sessionFactory == null ) {

	        serviceRegistry = new StandardServiceRegistryBuilder()
	                .configure("hibernate.cfg.xml")
	                .build();

	        sessionFactory = new MetadataSources( serviceRegistry )
	                    .buildMetadata()
	                    .buildSessionFactory();
	    }
	    return sessionFactory;
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		session = getSessionFactory().openSession();
		preparar();
		DaemonController aceptador = new DaemonController(servidor, msgs, commandas, index, substartlot, entradas, clientes);
		DaemonGeneradorLotes dgl = new DaemonGeneradorLotes();
		dgl.setDaemon(true);
		dgl.start();
		aceptador.start();
		ClienteDao ldd = new ClienteDao();
		System.out.println("Servidor online!");
		boolean final_push = true;
		while (true){
            	if(substart) {
            		ArrayList<Lote> listitems = new ArrayList<Lote>();
            		listitems.addAll(new LicitacionesDao().get(1).getLote());
            		for(Lote loe:listitems) {
            			if(!loe.isVendido()) {
            				lotes_sub.add(loe);
            			}
            		}
            		
            		substart = false;
            		msgs = "SUBASTA: La subasta se va a iniciar!";
            	}
            	
            	if(!lotes_sub.isEmpty() && !substart) {
            		le = lotes_sub.get(0);
            	} else {
            		msgs = "SUBASTA: No hay mas lotes para subastar...";
            	}
            	
            	if(le != null && !substartlot) {
            		msgs = "SUBASTA: Se va a subastar el lote: "+ le.getId() + " con nombre: "+ le.getNombre() + ", empezando por un valor de "+ le.getPrecio()+ "€."
            				+ "La subasta del lote empezara en 30s.";
            			sleep(15000);
            			msgs = "SUBASTA: Quedan 15s para que se inice la subasta del lote.";
            			sleep(10000);
            			msgs = "SUBASTA: La subasta del lote se va a inicar en 5 segundos...";
            			sleep(5000);
            			msgs = "SUBASTA: La subasta del lote ha empezado!";
            			substartlot = true;
            			start = System.currentTimeMillis();
            			end_sub = start + 100000;
            			last_time = start + 20000;
            	}
            	
            	if(substartlot) {
            		start = System.currentTimeMillis();
            		bid_time = System.currentTimeMillis();
            	}
            	
            	Cliente actual = null;
            	if(!entradas.isEmpty() && substartlot && start < end_sub && bid_time < last_time) {
            		if(entradas.size() != current) {
            			final_push = true;
            			current++;
            			String client = entradas.get(current-1).split(":")[0];
            			List<Cliente> lc = new ClienteDao().list();
            			actual = new Cliente();
            			for(Cliente c:lc) {
            				if(c.getUsuario().equals(client)) {
            					actual = c;
            					break;
            				}
            			}
            			int max = Integer.parseInt(entradas.get(current-1).split(":")[1]);
            			if(max >= le.getPrecio()) {
            				if(max > le.getPrecioadjudicado()) {
                				le.setPrecioadjudicado(max);
                				le.setCliente(actual);
                				msgs = "SUBASTA: " + actual.getUsuario() + ": ha pujado por un valor de: " + max + "€.";
                			}
            			}
            			bid_time = System.currentTimeMillis();
            			last_time = bid_time + 20000;
            		} else if(last_time - bid_time <= 5000 && final_push) {
            			msgs = "SUBASTA: Si nadie puja en 5s se pasara a la siguente puja.";
            			final_push = false;
            		}
            	} else if(start != 0 && substartlot && (start >= end_sub || bid_time >= last_time)) {
            		substartlot = false;
            		if(bid_time >= last_time) {
            			msgs = "SUBASTA: NADIE HA PUJADO DURANTE LOS ULTIMOS 20 SEGUNDOS PARA EL LOTE: " + le.getNombre() + ".";
            			if(le.getCliente() != null) {
            				msgs += "\nEl ganador de este lote ha sido:" + le.getCliente().getUsuario() + ", con un precio de: " + le.getPrecioadjudicado() + "€";
            				actual = le.getCliente();
            				le.setVendido(true);
            				actual.setSactual(actual.getSactual() - le.getPrecioadjudicado());
                			ldd.saveOrUpdate(actual);
            			}
            		} else if(le.getCliente() == null){
            			msgs = "SUBASTA: SE HA TERMINADO EL TIEMPO PARA EL LOTE " + le.getNombre() + "."
                				+ "\nNadie ha pujado por el lote "+ le.getNombre();
            		} else {
            			le.setVendido(true);
            			msgs = "SUBASTA: SE HA TERMINADO EL TIEMPO PARA EL LOTE " + le.getNombre() + "."
            					+ "\nEl ganador de este lote ha sido:" + le.getCliente().getUsuario() + ", con un precio de: " + le.getPrecioadjudicado()+ "€";
            			actual = le.getCliente();
            			actual.setSactual(actual.getSactual() - le.getPrecioadjudicado());
            			ldd.saveOrUpdate(actual);
            		}
            		LoteDao ld = new LoteDao();
            		LicitacionesDao lidao = new LicitacionesDao();
            		ld.saveOrUpdate(le);
            		if(!lidao.get(1).getLote().contains(le)) {
            			lidao.get(1).getLote().add(le);
            			lidao.saveOrUpdate(lidao.get(1));
            		}
            		
            		if(lotes_sub.size() > 0) {
            			lotes_sub.remove(0);
            		}
            		for(int i = 0; i < current; i++) {
            			entradas.remove(i);
            		}
            		current = 0;
            	}
        }
	}
	
	public static void preparar() throws IOException {
		servidor = new ServerSocket(8888);
		commandas.add("/help");
		commandas.add("/login");
		commandas.add("/register");
		commandas.add("/push");
		commandas.add("/logout");
		commandas.add("/get");
		commandas.add("/turn");
		commandas.add("/msg");
		commandas.add("/chat");
		int i = 0;
		for(String com:commandas) {
			index.put(com, i);
			i++;
		}
	}
	
}
