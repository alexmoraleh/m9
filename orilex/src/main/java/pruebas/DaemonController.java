package pruebas;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;

public class DaemonController extends Thread{
	
	ServerSocket servidor;
	public volatile String msgs;
	ArrayList<String> comands;
	Map<String, Integer> index;
	public volatile boolean substartlot;
	ArrayList<String> entradas;
	Map<String, ArrayList<String>> clientes;
	
	public DaemonController(ServerSocket servidor, String msgs, ArrayList<String> commandas, Map<String, Integer> index, boolean substartlot, ArrayList<String> entradas, Map<String, ArrayList<String>> clientes) {
		this.servidor = servidor;
		this.msgs = msgs;
		this.comands = commandas;
		this.index = index;
		this.substartlot = substartlot;
		this.entradas = entradas;
		this.clientes = clientes;
	}
	
	public void run() {
		while(true) {
			try {
				Socket client = servidor.accept();
				Commands Assistant = new Commands(client, msgs, comands, index, substartlot, entradas, clientes);
				Assistant.start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
		}
	}
}
