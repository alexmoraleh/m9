package dao;

import java.util.List;

import mapping.*;

public interface ILoteDao extends lGenericDao<Lote, Integer>{
	
	void saveOrUpdate(Lote c);

	Lote get(Integer id);

	List<Lote> list();

	void delete(Lote id);

}
