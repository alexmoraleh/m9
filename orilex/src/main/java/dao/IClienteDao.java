package dao;

import java.util.List;
import mapping.*;

public interface IClienteDao extends lGenericDao<Cliente, Integer>{

	void saveOrUpdate(Cliente c);

	Cliente get(Integer id);

	List<Cliente> list();

	void delete(Cliente id);

}
