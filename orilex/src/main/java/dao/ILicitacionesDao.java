package dao;

import java.util.List;
import mapping.*;

public interface ILicitacionesDao extends lGenericDao<Licitaciones, Integer>{
	
	void saveOrUpdate(Licitaciones c);

	Licitaciones get(Integer id);

	List<Licitaciones> list();

	void delete(Licitaciones id);
}
