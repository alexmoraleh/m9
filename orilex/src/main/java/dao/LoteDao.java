package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import mapping.*;

public class LoteDao extends GenericDao<Lote, Integer> implements ILoteDao{

	@Override
	public void delete(Lote id) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			Lote entity = (Lote) session.get(getEntityClass(), id.getIdProducto());
			session.delete(entity);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
		
	}

}
