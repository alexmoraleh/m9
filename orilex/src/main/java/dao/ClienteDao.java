package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import mapping.*;
public class ClienteDao extends GenericDao<Cliente, Integer> implements IClienteDao{

	@Override
	public void delete(Cliente id) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			Cliente entity = (Cliente) session.get(getEntityClass(), id.getId());
			session.delete(entity);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
		
	}

}
