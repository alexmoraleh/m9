package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import mapping.*;
public class LicitacionesDao extends GenericDao<Licitaciones, Integer> implements ILicitacionesDao{

	@Override
	public void delete(Licitaciones id) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			Licitaciones entity = (Licitaciones) session.get(getEntityClass(), id.getId());
			session.delete(entity);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
		
	}

}
