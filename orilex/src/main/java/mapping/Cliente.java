package mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ies
 *
 */
@Entity
@Table(name="cliente")
public class Cliente {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected int id;
	@Column
	String usuario;
	@Column
	String pass;
	@Column
	float sinicial;
	@Column
	float sactual;
	
	public Cliente() {
		super();
	}
	public Cliente(String usuario, String pass, float sinicial) {
		this();
		this.usuario = usuario;
		this.pass = pass;
		this.sinicial = sinicial;
		this.sactual = sinicial;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public float getSinicial() {
		return sinicial;
	}
	public void setSinicial(float sinicial) {
		this.sinicial = sinicial;
	}
	public float getSactual() {
		return sactual;
	}
	public void setSactual(float sactual) {
		this.sactual = sactual;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean login(String usr, String pass) {
		if(usuario.equals(usr)&&this.pass.equals(pass)) {
			return true;
		}
		return false;
	}

}
