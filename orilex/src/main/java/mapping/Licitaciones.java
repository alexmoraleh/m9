package mapping;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="licitaciones")
public class Licitaciones {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_licit")
	int id;//Autonumerico si o si
	//@OneToOne(cascade = CascadeType.PERSIST)
	//@JoinColumn(name="lote", nullable=false) Creo que no hace falta, seguro que me equivoco como siempre xd

	//@JoinColumn(name="lote")
//	https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/
	@OneToMany(mappedBy = "licitacion", fetch=FetchType.EAGER,
	        cascade = CascadeType.ALL,
	        orphanRemoval = true)//(cascade = {CascadeType.REFRESH})
	private Set<Lote> lote =new HashSet<Lote>();

	@OneToOne//Pon el joincolumn si hace falta
	Cliente cliente;//-.-
	@Column
	int importe;//No se que pollas es esto pero me da miedo preguntarle a G. Arreglado, antes era IDImport...
	@Column
	boolean accept;//Si el proceso es aceptado
	
	public Licitaciones() {
		super();
		accept=false;
	}
	public Licitaciones(Set<Lote> lote, Cliente cliente, int idImport, boolean accept) {
		this();
		this.lote = lote;
		this.cliente = cliente;
		this.importe = idImport;
		this.accept = accept;
	}
	
	public Licitaciones(Cliente cliente, int importe, boolean accept) {
		this();
		this.cliente = cliente;
		this.importe = importe;
		this.accept = accept;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Set<Lote> getLote() {
		return lote;
	}
	public void setLote(Set<Lote> lote) {
		this.lote = lote;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public int getImporte() {
		return importe;
	}
	public void setImporte(int idImport) {
		this.importe = idImport;
	}
	public boolean isAccept() {
		return accept;
	}
	public void setAccept(boolean accept) {
		this.accept = accept;
	}
	
}
