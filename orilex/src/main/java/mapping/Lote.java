package mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="lote")
public class Lote {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_lote")
	int id;//Autonumerico (como se hace esa mierda)
	
	@Column//Hacer un one to many
	int idProducto;//Metemos la tabla productos?? En caso de que si cambiar int por Producto
	@Column
	String nombre;
	@Column
	float precio;
	@Column
	@JoinColumn(nullable=true)
	float precioadjudicado=0;

	@ManyToOne
	@JoinColumn(name="cliente", nullable=true)
	Cliente cliente;//Si se complica la cosa guarda el id del cliente y tirando xd
	
	@Column
	boolean vendido;
	
//	@ManyToOne
//	@JoinColumn(name="licitaciones")
	@ManyToOne
    @JoinColumn(name = "id_licit")
	private Licitaciones licitacion;
	
	public Lote() {
		super();
		cliente=null;
		this.vendido = false;
	}
	public Lote(int idProducto, float precio, String nombre, Licitaciones l) {
		this();
		this.idProducto = idProducto;
		this.precio = precio;
		this.nombre=nombre;
		this.licitacion=l;
		this.vendido = false;
	}
	
	
	
	public boolean isVendido() {
		return vendido;
	}
	public void setVendido(boolean vendido) {
		this.vendido = vendido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public float getPrecioadjudicado() {
		return precioadjudicado;
	}
	public void setPrecioadjudicado(float precioadjudicado) {
		this.precioadjudicado = precioadjudicado;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}	
}
