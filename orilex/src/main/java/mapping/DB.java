package mapping;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DB {

	private static String url = "jdbc:mysql://localhost:3306/ttb";
	private static String user = "root";
	private static String passwd = "super3";
	
	private static Connection con = null;
	
	public static Connection connection() throws SQLException {
		if (con==null) {
			con = DriverManager.getConnection(url, user, passwd);
		}
		return con;
	}
	
	public static void closeConnection() throws SQLException {
		if (con!=null) {
			try {
				con.close();
			} finally {
				con = null;
			}
		}
	}
	
	private DB() {}
	
}
