package lootro;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import mapping.Cliente;
import mapping.DB;
import mapping.Licitaciones;
import mapping.Lote;

public class Programa {
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;
	public static synchronized SessionFactory getSessionFactory() {
	    if ( sessionFactory == null ) {

	        // exception handling omitted for brevityaa

	        serviceRegistry = new StandardServiceRegistryBuilder()
	                .configure("hibernate.cfg.xml")
	                .build();

	        sessionFactory = new MetadataSources( serviceRegistry )
	                    .buildMetadata()
	                    .buildSessionFactory();
	    }
	    return sessionFactory;
	}
		//SET GLOBAL time_zone = '+1:00';
	public static void main(String[] args) {
		session = getSessionFactory().openSession();
		//abrimos PRIMERA transaccion. Eso se hace siempre.
		session.beginTransaction();
		Cliente c=new Cliente("alex","super3",5000);
		Cliente u=new Cliente("oriol", "super3", 5000);
		Set<Lote> l = new HashSet<Lote>();
		Licitaciones li=new Licitaciones(l, c, 2400, true);
		session.saveOrUpdate(li);
//		for(int x=0;x<10;x++) {
			Lote lo=new Lote(1,2400,"Pastel de Pere",li);
			l.add(lo);
			session.saveOrUpdate(lo);
			lo=new Lote(2,1500,"Diabolo shiny",li);
			l.add(lo);
			session.saveOrUpdate(lo);
//		}
		li.setLote(l);
		session.saveOrUpdate(c);
		session.saveOrUpdate(u);
		//session.saveOrUpdate(l);
		
		
	}
}
