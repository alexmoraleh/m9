package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	
	private static final String IP_SERVER = "10.1.6.59";
	private static Scanner sc = new Scanner(System.in);
	private static ClientRecibir entrada;
	
	public static void main(String[] args) {

		try {
			System.out.println("Connectant...");
			InetAddress ipAddress = InetAddress.getByName(IP_SERVER);
			Socket connexio = new Socket(IP_SERVER, 8888);
			entrada = new ClientRecibir(connexio);
			entrada.start();
			System.out.println("Connectat!");
			
			PrintWriter txStream = new PrintWriter(connexio.getOutputStream(),
					true);
			while (true) {
				String tx = sc.nextLine();
				txStream.println(tx);
			}
		} catch (IOException e) {
			System.out.println("Error no esperat: " + e.getMessage());
			e.printStackTrace();
		}

	}
}
