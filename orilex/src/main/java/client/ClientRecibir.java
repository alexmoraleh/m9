package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientRecibir extends Thread {
	
	private BufferedReader rxStream;
	
	public ClientRecibir(Socket client) throws IOException {
		rxStream = new BufferedReader(new InputStreamReader(client.getInputStream()));
	}
	
	public void run() {
		while(true) {
			try {
				String entrada = rxStream.readLine();
				if(entrada != null) {
					System.out.println(entrada);
				} else {
					System.out.println("Adeu!");
					break;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			}
		}
	}
}
