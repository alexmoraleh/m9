package TOP100;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class Jugador extends Thread{
	Random rnd=new Random();
	int aux;
	boolean exit=true;
	Taulell t;
	Semaphore s;
	
	Jugador(Semaphore s, Taulell t){
		this.s=s;
		this.t=t;
	}
	
	@Override
	public void run(){
		while(exit) {
			try {
				s.acquire();
				if(t.getGanador()!="") exit=false;
				aux=rnd.nextInt(6)+1;
				System.out.println("\"Me toca\" dijo "+this.getName()+" y me ha salido un "+aux);
				t.setVariable(t.getVariable()+aux);
				if(t.getVariable()>=100) {
					t.setGanador(this.getName());
					s.release();
					exit=false;
				}
				else {
					s.release();
					if(aux%2==0)sleep(1000);
					else sleep(aux*1000);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
