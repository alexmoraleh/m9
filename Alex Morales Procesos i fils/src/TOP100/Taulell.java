package TOP100;

import java.util.Arrays;
import java.util.concurrent.Semaphore;

public class Taulell extends Thread{
	int variable=0;
	String ganador="";
	Semaphore s;
	

	public Taulell(Semaphore sem) {
		// TODO Auto-generated constructor stub
		s=sem;
	}

	public int getVariable() {
		return variable;
	}

	public void setVariable(int variable) {
		this.variable = variable;
	}

	public String getGanador() {
		return ganador;
	}

	public void setGanador(String ganador) {
		this.ganador = ganador;
	}
	
	@Override
	public void run() {
		while(ganador=="") {
			try {
				s.acquire();
				System.out.println("Total: "+variable);
				s.release();
				sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("El Ganador es "+ganador+" con un total de "+variable);
	}
}
