package TOP100;

import java.util.concurrent.Semaphore;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Semaphore sem=new Semaphore(1);
		Taulell t=new Taulell(sem);
		Jugador j1=new Jugador(sem,t);
		Jugador j2=new Jugador(sem,t);
		
		j1.setName("Jugador 1");
		j2.setName("Jugador 2");
		
		t.start();
		j1.start();
		j2.start();
	}

}
