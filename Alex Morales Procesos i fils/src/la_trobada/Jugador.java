package la_trobada;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class Jugador extends Thread{
	
	int posicio=0;
	int cont=0;
	boolean asc=true;
	Taulell t;
	Semaphore taulellaccess;
	Semaphore noti;
	Random rnd=new Random();
	
	//Jugador ASC = 1, marcas = 11
	//Jugador DES = 2, marcas = 22
	
	public Jugador(Taulell t, Semaphore s, Semaphore noti) {
		this.t=t;
		taulellaccess=s;
		this.noti=noti;
	}
	
	public Jugador(Taulell t, int x, Semaphore s, Semaphore noti) {
		this(t,s,noti);
		posicio=x;
		asc=false;
	}
	
	@Override
	public void run(){
		try {
			while (t.getGuanyador()=="") {
				System.out.println(t);
				if(asc) ASC();
				else DES();
				if(t.getGuanyador()!="")break;//matar thread
			}
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private synchronized void ASC() throws InterruptedException {
		taulellaccess.acquire();
		if(posicio!=0) t.getTaulell()[posicio]=11;//TODO igual esto se borra
		posicio=posicio+(rnd.nextInt(6)+1);
		if(posicio>=t.getTaulell().length-1) {
			t.setGuanyador("Jugador 1");
			System.out.println(t.getGuanyador());
			t.notifyt();
			interrupt();
		}
		else {
			if(t.getTaulell()[posicio]!=2&&t.getTaulell()[posicio]!=22&&t.getTaulell()[posicio]!=9||t.getTaulell()[posicio]==0) {
				t.getTaulell()[posicio]=1;
				taulellaccess.release();
				cont++;
				System.out.println("1 sleep");
				Thread.sleep(2000);
			}
			else {
				if(t.getTaulell()[posicio]==22) {
					t.getTaulell()[posicio]=1;
					taulellaccess.release();
					System.out.println("1 sleep encontrado");
					Thread.sleep(cont*1000);
					cont++;
				}
				else {
					if(t.getTaulell()[posicio]==9) {
						System.out.println("1 banned");
						noti.acquire();
					}
					else {
						for(int cont=0;cont<t.getTaulell().length;cont++) {
							if(t.getTaulell()[cont]==11||t.getTaulell()[cont]==1) t.getTaulell()[cont]=0;
						}
						taulellaccess.release();
						posicio=0;
						System.out.println("WTF 1");
						Thread.sleep(2000);
					}
				}
			}
		}
	}
	private synchronized void DES() throws InterruptedException {
		taulellaccess.acquire();
		if(posicio!=t.getTaulell().length-1) t.getTaulell()[posicio]=22;//TODO igual esto se borra
		posicio=posicio-(rnd.nextInt(6)+1);
		if(posicio<=0) {
			System.out.println("he llegado");
			t.setGuanyador("Jugador 2");
			t.notifyt();
		}
		else {
			if(t.getTaulell()[posicio]!=1&&t.getTaulell()[posicio]!=11&&t.getTaulell()[posicio]!=9||t.getTaulell()[posicio]==0) {
				t.getTaulell()[posicio]=2;
				taulellaccess.release();
				cont++;
				System.out.println("2 sleep");
				Thread.sleep(2000);
			}
			else {
				if(t.getTaulell()[posicio]==11) {
					t.getTaulell()[posicio]=2;
					taulellaccess.release();
					System.out.println("2 sleep encontrado");
					Thread.sleep(cont*1000);
					cont++;
				}
				else {
					if(t.getTaulell()[posicio]==9) {
						System.out.println("2 banned");
						noti.acquire();
					}
					else {
						for(int cont=0;cont<t.getTaulell().length;cont++) {
							if(t.getTaulell()[cont]==22||t.getTaulell()[cont]==2) t.getTaulell()[cont]=0;
						}
						taulellaccess.release();
						posicio=99;
						System.out.println("WTF 2");
						Thread.sleep(2000);
					}
				}
			}
		}
	}
	public synchronized void nowait() throws InterruptedException {
		notifyAll();
	}
}
