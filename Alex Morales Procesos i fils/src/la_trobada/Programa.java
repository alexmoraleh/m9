package la_trobada;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Semaphore taulellacc=new Semaphore(1);
		Semaphore zom=new Semaphore(1);
		Semaphore noti=new Semaphore(0);
		Taulell taulell=new Taulell();
		Jugador j1=new Jugador(taulell, taulellacc, noti);
		Jugador j2=new Jugador(taulell, taulell.getTaulell().length-1, taulellacc, noti);
		ArrayList<Jugador>jl = new ArrayList<>();
		jl.add(j1);
		jl.add(j2);
		Zombi z=new Zombi(taulell, zom, jl, noti);
		
		taulell.start();
		z.setDaemon(true);
		z.start();
//		j1.setDaemon(true);
		j1.start();
//		j2.setDaemon(true);
		j2.start();
		
	}

}
