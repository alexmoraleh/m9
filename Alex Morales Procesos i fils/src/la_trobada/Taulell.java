package la_trobada;

import java.util.Arrays;

public class Taulell extends Thread{
	int taulell[]=new int[100];
	String guanyador="";
	//TODO hacer que taulell sea un thread y se quede en wait hasta que un jugador llene el string, al despertar dice quien ha ganado

	public int[] getTaulell() {
		return taulell;
	}

	public void setTaulell(int[] taulell) {
		this.taulell = taulell;
	}

	public String getGuanyador() {
		return guanyador;
	}

	public void setGuanyador(String guanyador) {
		this.guanyador = guanyador;
	}
	
	@Override
	public void run(){
		try {
			espera();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Taulell esta en wait");
		}
	}

	private synchronized void espera() throws InterruptedException {
		wait();
		System.out.println("El guanyador es: "+guanyador);
	}
	public synchronized void notifyt() {
		notifyAll();
	}
	@Override
	public String toString() {
		return "Situaci�:\n" + Arrays.toString(taulell);
	}
	
}
