package la_trobada;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Zombi extends Thread{


	Random rnd=new Random();
	int num;
	Taulell t;
	Semaphore s;
	Semaphore noti;
	ArrayList<Jugador> jl;
	
	public Zombi(Taulell t, Semaphore s, ArrayList<Jugador>jl, Semaphore noti) {
		this.t=t;
		this.s=s;
		this.jl=jl;
		this.noti=noti;
	}
	
	@Override
	public void run() {
		while(true) {
			try {
				
				
				num=rnd.nextInt(20)+8;
				System.out.println("Ojo que baneo. Dijo Zombie "+num);
				s.acquire();
				if(t.getGuanyador()!="") {
					System.out.println(t);
					break;
				}
				for(int cont=0;cont<t.getTaulell().length;cont++) {
					if(t.getTaulell()[cont]==9)t.getTaulell()[cont]=0;
				}
				noti.release();
//				for(Jugador j:jl) {
//					j.nowait();
//					System.out.println("notify check");
//				}
				
				
				for(int cont=0;cont<t.getTaulell().length;cont++) {
					if(cont%num==0)t.getTaulell()[cont]=9;
					cont++;
				}
				s.release();
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public synchronized void notifyj() {
		notifyAll();
	}
}
