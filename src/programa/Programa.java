package programa;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JFileChooser;

import tastat.Client;
import tastat.Comanda;
import tastat.DiariMoviments;
import tastat.Magatzem;
import tastat.Operari;
import tastat.OrdreCompra;
import tastat.Producte;
import tastat.Proveidor;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.interfaces.RSAKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class Programa implements Serializable {//Falta 5,6,8
	private static PublicKey publicKey;
	private static PrivateKey privateKey;
	
	//Versión symmetric
	private static SecretKeySpec secretKey;
	private static Cipher cipher;

	private static Cipher rsa;
	private static final long serialVersionUID = 1L;

	static Scanner input;
	static Magatzem elMeuMagatzem = new Magatzem(new ArrayList<Producte>(), new ArrayList<Client>(),
			new ArrayList<Comanda>(), new ArrayList<Proveidor>(), new ArrayList<Operari>(),
			new ArrayList<DiariMoviments>(), new ArrayList<OrdreCompra>());

	public static void main(String[] args) throws Exception {

		// 1.- Generació d'un magatzem petitó

		Generadors.generarClients(elMeuMagatzem.getClients());
		Generadors.generarProveidors(elMeuMagatzem.getProveidors());
		Generadors.generarProductes(elMeuMagatzem);
		Generadors.generarComandes(elMeuMagatzem);
		// Generadors.veureTot(elMeuMagatzem);

		menu(elMeuMagatzem);

	}

	static void menu(Magatzem mgz) throws Exception {
		input = new Scanner(System.in);
		FileDialog dialog;
		File f;
		int seleccionado = 0;
		// System.out.println("\n\n\n");
		do {
			seleccionado = displayMenu();

			switch (seleccionado) {
			case 1:
				// TODO generar
    			Generadors.generarComandes(mgz);
				break;
			case 2:
				// TODO ver comandas
				mgz.veureComandes();
				break;
			case 3:
				File writeClar = guardar("comandesClar.dat");
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(writeClar));

				for (Comanda c : elMeuMagatzem.getComandes()) {
					oos.writeObject(c);
				}
				oos.flush();
				oos.close();

				// TODO generar comandas serializadas
				break;
			case 4:
				File readClar = abrir("Seleccioni l'arxiu a obrir");
				FileInputStream fis = new FileInputStream(readClar);
				ObjectInputStream ois = new ObjectInputStream(fis);

				Object com = ois.readObject();
				try {
					while (com != null) { 
						if(com instanceof Comanda) {
							elMeuMagatzem.getComandes().add((Comanda)com);
						}	
						com = ois.readObject();
					}
					ois.close();
					fis.close();
				} catch(EOFException e) {
					System.out.println("END OF FILE.");
				}

				// TODO cargar las comandas
				break;
			case 5:
				if (privateKey==null) System.out.println("No hi ha clau privada encara, ves-hi a la opció 10."); 
				else {
					
	    		    //if (signData(abrir("Seleccioni l'arxiu a obrir"))) {
					generateHash();
	    		    	
	    		    //}
				}
			// TODO
			break;
			case 6:
				if (publicKey==null) System.out.println("No hi ha clau privada encara, ves-hi a la opci� 10."); 
				else {
					//if (validateSignature(abrir("Fitxer encriptat"))) System.out.println("Firma correcta");
					String hashA = generateHash();
					String hashB = decrypt("hashResum.dat");
					
					if(hashA == hashB) {
						File readClar2 = abrir("Seleccioni l'arxiu a obrir");
						FileInputStream fis2 = new FileInputStream(readClar2);
						ObjectInputStream ois2 = new ObjectInputStream(fis2);

						Object com2 = ois2.readObject();
						try {
							while (com2 != null) { 
								if(com2 instanceof Comanda) {
									elMeuMagatzem.getComandes().add((Comanda)com2);
								}	
								com2 = ois2.readObject();
							}
							ois2.close();
							fis2.close();
						} catch(EOFException e) {
							System.out.println("END OF FILE.");
						}
					}
				}
				// TODO
				break;
			case 7:
				if(publicKey!=null) {
					if(secretKey!=null) {
						//cifrarsymm(guardar("symmetric.dat"));
						cifrarsymm(abrir("Arxiu a cifrar."));
					}
					else System.out.println("No hi ha clau simetrica, ves-hi a la opció 15");
				}
				else System.out.println("No ha inserit la clau publica, ves-hi a la opció 13."); 
				
				//guardarComandesEncr(publicKey);
				
				break;
			case 8:
				//Si que funcionava, encriptava mal, ahora queda ver como se guarda la llave simetrica ya desencriptada
				if(publicKey!=null) {
					descifrarsymm();
				}
				else System.out.println("No ha inserit la clau publica, ves-hi a la opció 13."); 
				//recibirComandasiEncr(privateKey);
				
				// TODO cargar comandas cifradas
				break;
			case 10:

				// Generar el par de claves
				KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
				KeyPair keyPair = keyPairGenerator.generateKeyPair();
				publicKey = keyPair.getPublic();
				privateKey = keyPair.getPrivate();

				// TODO generar par de llaves
				break;
			case 11:

				// Se salva y recupera de fichero la clave publica
				saveKey(publicKey, guardar("publickey.dat").getAbsolutePath());
				
				// TODO guardar llave publica
				break;
			case 12:

				// Se salva y recupera de fichero la clave privada
				saveKey(privateKey, guardar("privatedat.dat").getAbsolutePath());
				

				// TODO guardar llave privada
				break;
			case 13:
				System.out.println("Escoge el fichero del cual quieres recuperar la clave.");
				loadKey(abrir("Escoge el fichero del cual quieres recuperar la clave.").getAbsolutePath());
				// TODO recuperar una llave
				break;
			case 14:
				System.out.println("Introduce el nombre del fichero que contenga las llaves para leerlas. ");
				readKeys(abrir("Introduce el nombre del fichero que contenga las llaves para leerlas. ").getAbsolutePath());
				// TODO ver llaves en un fichero
				break;
			case 15:
				if(createkey("My53cr3tPa55w0rd", 16, "AES")) System.out.println("Clau generada correctament.");
				else System.out.println("Hi ha hagut algun error durant la creació.");
				break;
			case 16:
				saveKey(secretKey, guardar("llavesimetrica.dat").getAbsolutePath());
				break;
			case 0:
				System.out.println("Bye");
				break;
			default:
				break;
			}
		} while (seleccionado != 0);
	}
	
	
	
	private static void descifrarsymm() throws IOException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		//Descifrar simetrica
		File f=abrir("Obrir clau simetrica");
		FileInputStream in = new FileInputStream(f);
		byte[] input = new byte[(int) f.length()];
		in.read(input);
		cipher=Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] symmbyte=cipher.doFinal(input);
		String symm=new String(symmbyte);
		System.out.println("Llave simetrica: "+symm);
		
		//Descifrar comandas
		f=abrir("Obrir fitxer comandes");
		in= new FileInputStream(f);
		input=new byte[(int) f.length()];
		in.read(input);
		SecretKeySpec secretKeySpec = new SecretKeySpec(symmbyte, "AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
		byte[] decryptedBytes = cipher.doFinal(input);
		String decryptedString = new String(decryptedBytes);
		System.out.println(decryptedString);
	}

	private static void cifrarsymm(File f) throws Exception {
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		writeToFile(f);
		 Cipher encrypt= Cipher.getInstance("RSA/ECB/PKCS1Padding");
		    // init with the *public key*!
		    encrypt.init(Cipher.ENCRYPT_MODE, publicKey);
		    // encrypt with known character encoding, you should probably use hybrid cryptography instead 
		    //byte[] iv = encrypt.getIV();
		    try (FileOutputStream fileOut = new FileOutputStream(guardar("SymmetricKeyEncrypted.dat"));
		    	      CipherOutputStream cipherOut = new CipherOutputStream(fileOut, encrypt)) {
		    	        //fileOut.write(iv);
		    	        cipherOut.write(secretKey.toString().getBytes());
		    }
//		saveKey(secretKey, guardar("symmetrickey.dat").getAbsolutePath());
	}

	private static void saveKey(Key key, String fileName) throws Exception {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
		oos.writeObject(key);
		oos.flush();
		oos.close();
	}
	public static void writeToFile(File f) throws IOException, IllegalBlockSizeException, BadPaddingException {
		FileInputStream in = new FileInputStream(f);
		byte[] input = new byte[(int) f.length()];
		in.read(input);

		FileOutputStream out = new FileOutputStream(guardar("symmetric.dat"));
		byte[] output = cipher.doFinal(input);
		out.write(output);

		out.flush();
		out.close();
		in.close();
	}
	/* private static PublicKey loadPublicKey(String fileName) throws Exception {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
		int numBtyes = ois.available();
		byte[] bytes = new byte[numBtyes];
		ois.read(bytes);
		ois.close();

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		KeySpec keySpec = new X509EncodedKeySpec(bytes);
		PublicKey keyFromBytes = keyFactory.generatePublic(keySpec);
		return keyFromBytes;
	} */

	//Symmetric
	public static boolean createkey(String secret, int length, String algorithm)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException {
		byte[] key = new byte[length];
		key = fixSecret(secret, length);
		secretKey = new SecretKeySpec(key, algorithm);
		cipher = Cipher.getInstance(algorithm);
		if(cipher!=null) return true;
		else return false;
	}
	private static byte[] fixSecret(String s, int length) throws UnsupportedEncodingException {
		if (s.length() < length) {
			int missingLength = length - s.length();
			for (int i = 0; i < missingLength; i++) {
				s += " ";
			}
		}
		return s.substring(0, length).getBytes("UTF-8");
	}
	//End Symmetric
	private static void loadKey(String fileName) throws Exception {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
		Object key = ois.readObject();
		if(key instanceof PublicKey) {
			publicKey = (PublicKey) key;
		} else {
			privateKey = (PrivateKey) key;
		}
	}
	
	private static void readKeys(String fileName) throws Exception {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
			Object key = ois.readObject();
			while(key != null) {
				
				if(key instanceof PublicKey) {
					PublicKey keyT = (PublicKey) key;
					System.out.println(keyT.toString());
				} else {
					PrivateKey keyA = (PrivateKey) key;
					System.out.println(keyA);
				}
				
				key = ois.readObject();
				
				
			}
		} catch (EOFException e) {
			System.out.println("END OF FILE.");
		}
		
	}

	public static int displayMenu() {
		System.out.println("1.- Generar Comandes");
		System.out.println("2.- Veure Comandes");
		System.out.println("3.- Generar Fitxer de Comandes per enviar en Clar");
		System.out.println("4.- Rebre Fitxer de Comandes en Clar");
		System.out.println("5.- Generar Fitxer de Comandes per enviar Signat");
		System.out.println("6.- Rebre Fitxer de Comandes Signat");
		System.out.println("7.- Generar Fitxer de Comandes Xifrat amb clau");
		System.out.println("8.- Rebre Fitxer de Comandes Xifrat");

		System.out.println("\n10.- Generar parell de Claus");
		System.out.println("11.- Gravar clau pública en fitxer");
		System.out.println("12.- Gravar clau privada en fitxer");
		System.out.println("13.- Recuperar clau de Fitxer");
		System.out.println("14.- Veure claus del Fitxer");
		System.out.println("15.- Generar clau Simetrica AES");
		System.out.println("16.- Gravar clau simetrica en fitxer");
		System.out.println("\n\n0.- Sortir");
		System.out.print("Opción: ");

		int seleccionado = -1;
		try {
			seleccionado = input.nextInt();
		} catch (Exception e) {

		}
		input.nextLine();
		return seleccionado;
	}

	//Versión antigua
	//	public static boolean signData(File f, PrivateKey priv) throws IOException {
//		byte[] data = Files.readAllBytes(f.toPath());
//    	byte[] signature = null;
//    	boolean bol = false;
//    	try {
//	    	Signature signer = Signature.getInstance("SHA1withRSA");
//	    	signer.initSign(priv);
//	    	signer.update(data);
//	    	signature = signer.sign();
//	    	if (signature!=null) {
//	    		try (FileOutputStream fos = new FileOutputStream(guardar("firma.dat").getAbsolutePath())) {
//	    			fos.write(signature);
//	    			bol=true;
//	    		}
//	    	}
//    	}
//    	catch (Exception e) {
//    		System.err.println("Error signant les dades: " + e);
//    	}
//    	return bol;
//    	MessageDigest messageDigest2 = MessageDigest.getInstance("SHA");
//    }
	public static String resumen(File f) throws IOException, NoSuchAlgorithmException {
		MessageDigest messageDigest2 = MessageDigest.getInstance("SHA");
		InputStream archivo = new FileInputStream( f );
        byte[] buffer = new byte[1];
        int fin_archivo = -1;
        int caracter;
   
        caracter = archivo.read(buffer);
        
        while( caracter != fin_archivo ) {
            
            // Pasa texto claro a la función resumen
            messageDigest2.update(buffer);
            caracter = archivo.read(buffer);
         }
        
        archivo.close();
        byte[] resumen2 = messageDigest2.digest(); // Genera el resumen SHA-1
        
        String m = "";
        for (int i = 0; i < resumen2.length; i++)
        {
           m += Integer.toHexString((resumen2[i] >> 4) & 0xf);
           m += Integer.toHexString(resumen2[i] & 0xf);
        }
        return m;
	}
	public static boolean signData(File f) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
		boolean bol = false;
		String m=resumen(f);
        System.out.println("Resumen SHA-1: " + m);
        
        
        //Aqui hay que encriptar con la privada la m y escribirlo en un archivo
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        byte[] iv = cipher.getIV();
        
        try (FileOutputStream fileOut = new FileOutputStream(guardar("resum.dat"));
        	      CipherOutputStream cipherOut = new CipherOutputStream(fileOut, cipher)) {
        	        fileOut.write(iv);
        	        cipherOut.write(m.getBytes());
        	    }
        //fin 
        
    	if(true) {//Si se firma todo correcto
    		bol=true;
    	}
    	
    	return bol;
    	
    }
	public static String generateHash() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		byte[] buffer= new byte[8192];
	    int count;
	    MessageDigest digest = MessageDigest.getInstance("SHA-256");
	    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(abrir("Obrir fitxer comandes")));
	    while ((count = bis.read(buffer)) > 0) {
	        digest.update(buffer, 0, count);
	    }
	    bis.close();

	    byte[] hash = digest.digest();
	    System.out.println();
	    
  
            // Convert byte array into signum representation 
            BigInteger no = new BigInteger(1, hash); 
  
            // Convert message digest into hex value 
            String hashtext = no.toString(16); 
  
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
  
            System.out.println("SHA: "+ hashtext);
  
        // For specifying wrong message digest algorithms
	    
	    
	    // specify mode and padding instead of relying on defaults (use OAEP if available!)
	    Cipher encrypt= Cipher.getInstance("RSA/ECB/PKCS1Padding");
	    // init with the *public key*!
	    encrypt.init(Cipher.ENCRYPT_MODE, privateKey);
	    // encrypt with known character encoding, you should probably use hybrid cryptography instead 
	    //byte[] iv = encrypt.getIV();
	    File f = guardar("hashResum.dat");
	    try (FileOutputStream fileOut = new FileOutputStream(f);
	    	      CipherOutputStream cipherOut = new CipherOutputStream(fileOut, encrypt)) {
	    	        //fileOut.write(iv);
	    	        cipherOut.write(hashtext.getBytes());
	    }
	    
	    return hashtext;
	}
	static String decrypt(String fileName) throws FileNotFoundException, IOException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchPaddingException {
	    String content;
	    
	    Cipher decrypt= Cipher.getInstance("RSA/ECB/PKCS1Padding");
	    
	    
	    try (FileInputStream fileIn = new FileInputStream(abrir("Obrir resum Hash"))) {
	        //byte[] fileIv = new byte[16];
	        //fileIn.read(fileIv);
	        decrypt.init(Cipher.DECRYPT_MODE, publicKey);
	 
	        try (
	                CipherInputStream cipherIn = new CipherInputStream(fileIn, decrypt);
	                InputStreamReader inputReader = new InputStreamReader(cipherIn);
	                BufferedReader reader = new BufferedReader(inputReader)
	            ) {
	 
	            StringBuilder sb = new StringBuilder();
	            String line;
	            while ((line = reader.readLine()) != null) {
	                sb.append(line);
	            }
	            content = sb.toString();
	        }
	 
	    }
	    return content;
	}
    public static boolean validateSignature(File f) throws IOException, NoSuchAlgorithmException{
    	String des="";
    	boolean isValid = false;
    	
    	//parte de desincriptacion, metelo en des
    	
    	String res=resumen(f);
    	
    	if(des.equals(res)) {
    		//Si coincideixen, carrega el fitxer de comandes (ja que no ha estat alterat)
    		//Entiendelo tu sabes....
    		isValid=true;
    	}
    	else {
    		System.out.println("Error, els resums no coincideixen.");
    	}
    	
	    
	   
	    return isValid;
    }
    
    public static File guardar(String s) {
    	//Esto es para escoger la carpeta destino
		System.out.println("Pot ser que s'hagi obert una finestra en segon plà...");
		FileDialog dialog;
		File f;
		dialog = new FileDialog((Frame)null, "Guardar Fitxer");
		dialog.toFront();
	    dialog.setMode(FileDialog.SAVE);
	    dialog.setFile(s);
	    dialog.setVisible(true);
	    f=dialog.getFiles()[0];
	    dialog.dispose();
	    return f;
    }
    public static File abrir(String s) {
    	//Esto es para escoger el fichero
		System.out.println("Pot ser que s'hagi obert una finestra en segon plà...");
		FileDialog dialog;
		File f;
		dialog = new FileDialog((Frame)null, s);
		dialog.toFront();
	    dialog.setMode(FileDialog.LOAD);
	    dialog.setVisible(true);
	    f=dialog.getFiles()[0];
	    dialog.dispose();
	    return f;
    }
}
