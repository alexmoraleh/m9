package tastat;

import java.util.Date;

public class DiariMoviments {
	protected int numMoviment;
	protected Date moment;
	protected Producte producte;
	protected int lot;
	protected char tipusMoviment;
	protected int quantitat;
	protected String observacions;
	protected Comanda comanda;
	protected OrdreCompra ordreCompra;
	protected boolean inlog=false; //Si ya se ha hecho un log sobre el se pone true
	
	DiariMoviments(){
		numMoviment = Generador.getNextDiariMoviment();
		moment = new Date();
	}
	//Mutex, zona exclusion multiple
	
	public DiariMoviments(Producte producte, int lot, char tipusMoviment, int quantitat,
			String observacions, Comanda comanda, OrdreCompra ordreCompra, boolean inlog) {//Full
		this();
		this.producte = producte;
		this.lot = lot;
		this.tipusMoviment = tipusMoviment;
		this.quantitat = quantitat;
		this.observacions = observacions;
		this.comanda = comanda;
		this.ordreCompra = ordreCompra;
		this.inlog = inlog;
	}

	public DiariMoviments(char tipusMoviment, int quantitat, String observacions, Comanda comanda) {//Movimiento Transporte
		this();
		this.tipusMoviment = tipusMoviment;
		this.quantitat = quantitat;
		this.observacions = observacions;
		this.comanda = comanda;
	}
	
	
	public DiariMoviments(Producte producte, char tipusMoviment, int quantitat, String observacions,
			OrdreCompra ordreCompra) { //Movimiento Reponer
		super();
		this.producte = producte;
		this.tipusMoviment = tipusMoviment;
		this.quantitat = quantitat;
		this.observacions = observacions;
		this.ordreCompra = ordreCompra;
	}
	
	public DiariMoviments(char tipusMoviment, String observacions) {
		super();
		this.tipusMoviment = tipusMoviment;
		this.observacions = observacions;
	}
	
	public DiariMoviments(Producte p, char c, int q, String o) {
		super();
		this.producte = p;
		this.tipusMoviment = c;
		this.quantitat = q;
		this.observacions = o;
	}

	public int getNumMoviment() {
		return numMoviment;
	}

	public void setNumMoviment(int numMoviment) {
		this.numMoviment = numMoviment;
	}

	public Date getMoment() {
		return moment;
	}

	public void setMoment(Date moment) {
		this.moment = moment;
	}

	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	public int getLot() {
		return lot;
	}

	public void setLot(int lot) {
		this.lot = lot;
	}

	public char getTipusMoviment() {
		return tipusMoviment;
	}

	public void setTipusMoviment(char tipusMoviment) {
		this.tipusMoviment = tipusMoviment;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}

	public String getObservacions() {
		return observacions;
	}

	public void setObservacions(String observacions) {
		this.observacions = observacions;
	}

	public Comanda getComanda() {
		return comanda;
	}

	public void setComanda(Comanda comanda) {
		this.comanda = comanda;
	}

	public OrdreCompra getOrdreCompra() {
		return ordreCompra;
	}

	public void setOrdreCompra(OrdreCompra ordreCompra) {
		this.ordreCompra = ordreCompra;
	}

	DiariMoviments(Producte p, int l, char tipusM, int q, String obs) {
		this();
		producte = p;
		lot = l;
		tipusMoviment = tipusM;
		quantitat = q;
		observacions = obs;
	}
	public Boolean getInlog() {return inlog;}
	
	@Override
	public String toString() {
		return "DiariMoviments [numMoviment=" + numMoviment + ", moment=" + moment + ", producte=" + producte + ", lot="
				+ lot + ", tipusMoviment=" + tipusMoviment + ", quantitat=" + quantitat + ", observacions="
				+ observacions + ", comanda=" + comanda + ", ordreCompra=" + ordreCompra + "]";
	}

	public String toString(Boolean x) {
		if(!inlog) {
			inlog=true; //Ahora mismo me da igual que boolean me venga
			return toString();
		}
		else return null;
	}
	
}