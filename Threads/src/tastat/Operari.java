package tastat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Operari extends Thread {
	private static Random rnd = new Random();
	protected int idOperari;
	protected String nomOperari;
	protected int numPastissos;
	protected Magatzem mgz;
	protected volatile boolean banned;
	protected Semaphore crdy;
	protected Semaphore prdy;
	protected Semaphore cprep;
	protected Semaphore drdy;
	protected Semaphore ordy;
	protected Semaphore enespera;

	Operari() {
		idOperari = Generador.getNextOperari();
		Thread.currentThread().setName(Integer.toString(idOperari));
		numPastissos = 0;
	}

	Operari(Magatzem mgz, Semaphore crdy, Semaphore prdy, Semaphore cprep, Semaphore drdy, Semaphore ordy, Semaphore enespera) {
		this();
		this.mgz = mgz;
		this.crdy = crdy;
		this.prdy = prdy;
		this.cprep = cprep;
		this.drdy = drdy;
		this.ordy = ordy;
		this.enespera = enespera;
	}

	public int getNumPastissos() {
		return numPastissos;
	}

	public void setNumPastissos(int numPastissos) {
		this.numPastissos = numPastissos;
	}

	Operari(String nom) {
		this();
		nomOperari = nom;
	}

	@Override
	public String toString() {
		return (idOperari + ": " + getNomOperari());
	}

	public int getIdOperari() {
		return idOperari;
	}

	public String getNomOperari() {
		return nomOperari;
	}

	public void setNomOperari(String nomOperari) {
		this.nomOperari = nomOperari;
	}

	public void run() {
		Producte p;
		try {
			Date date = new Date();   // given date
			Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
			calendar.setTime(date);
			int pass = 0;
			int ctime = calendar.get(Calendar.SECOND);
			for (;;) {
				Date moment = new Date();
				Calendar get = GregorianCalendar.getInstance();
				get.setTime(moment);
				int cmoment = get.get(Calendar.SECOND);
				if(cmoment < ctime) {
					pass += 60 - ctime + cmoment; 
				} else {
					pass += (cmoment - ctime);
				}
				ctime = cmoment;
				if(pass >= 60) {
					System.out.println(this.getId() + " O: em poso a descansar 5 minuts.");
					pass = 0;
					sleep(5000);
				}
				
				if (banned) break;
				boolean rdy;
				int cont = 0;

				// TODO Si, esto va a retrasaar bastante los operarios, soy consciente de ello,
				// pero tambien soy un vago
				
				
				//TODO Hay que hacer que los operarios hagan  los procesos correspondientes a los productos(limpiarlos, etc.) descritos en la practica
				cprep.acquire();
				for (Comanda c : mgz.getComandesp()) {
					if (c.getEstat() == ComandaEstat.TRANSPORT) {
						cont++;
					}
				}
				//Comprueba si hay mas cajas de las que pueden hacer
				if ((mgz.getComandesp().size() - cont) > 2) {
					rdy = false;
				} else {
					rdy = true;
				}
				cprep.release();
				
				
				
				if (!rdy) {
					System.out.println("Hi han " + (mgz.getComandesp().size() - cont) + " en espera.");
					Thread.sleep(5000);
				} else {
					if (mgz.getComandes().size() > 0) {
						Comanda cm = mgz.getComanda();//wen truco
						if (cm.getEstat() == ComandaEstat.PENDENT) {
							prdy.acquire();
							p = cm.rebaixarUnitat();
							prdy.release();
							if (p != null) {
								System.err.println("S�c " + getIdOperari() + " O i comen�o a fabricar: " + p.getNomProducte() + ". Comanda: "+ cm.getIdComanda());//TODO aqui hace sus movidas, copiar del operario antiguo
								
								opTrabajador(p);
								cm.unitatOK(p);
							}
							
							crdy.acquire();
							if (cm.isFinish() && cm.getEstat() == ComandaEstat.PENDENT) {
								System.out.println("Soc " + getIdOperari() + "T i ja esta la comanda "
										+ cm.getIdComanda() + ": PREPARADA.");
								cm.setEstat(ComandaEstat.PREPARADA);
								
								drdy.acquire();
								mgz.getMoviments().add(new DiariMoviments('P', 1, "Comanda ja preparada", cm));
								drdy.release();
								mgz.getComandes().remove(cm);
								mgz.getComandesp().add(cm);
								Thread.sleep(rnd.nextInt(2000));
							}
							crdy.release();
						}
					} else {
						Thread.sleep(rnd.nextInt(2000));

					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		};
	}
	private void opTrabajador(Producte p) throws InterruptedException {
		for(ProducteFabricacio ps:p.getFabricacio()) {
			ArrayList<Producte> ary = new ArrayList<Producte>();
			ary.addAll(mgz.getProductes());//Esto resta stock
			p.getWait().acquire();
			if(ary.indexOf(ps.getProducte())!=-1) {
				p.getWait().release();
				if(ary.get(ary.indexOf(ps.getProducte())).getStock() - ps.getQuantitat() < ary.get(ary.indexOf(ps.getProducte())).getStockMinim()) {
					ordy.acquire();
					mgz.getCompres().add(new OrdreCompra(ary.get(ary.indexOf(ps.getProducte())).getProveidor(), ps.getProducte(), ary.get(ary.indexOf(ps.getProducte())).getStockMinim()));
					mgz.setHiHanOrdres(mgz.getHiHanOrdres()+1);
					drdy.acquire();
					mgz.getMoviments().add(new DiariMoviments(ps.getProducte(), 'S', ary.get(ary.indexOf(ps.getProducte())).getStockMinim() , "Entrada stock ", new OrdreCompra(ary.get(ary.indexOf(ps.getProducte())).getProveidor(), ps.getProducte(), ary.get(ary.indexOf(ps.getProducte())).getStockMinim())));
					drdy.release();
					p.getWait().acquire();
					while(ary.get(ary.indexOf(ps.getProducte())).getStock() - ps.getQuantitat() < ary.get(ary.indexOf(ps.getProducte())).getStockMinim()) {
					}
					p.getWait().release();
					ordy.release();
				} 
				ary.get(ary.indexOf(ps.getProducte())).restStock();
				drdy.acquire();
				mgz.getMoviments().add(new DiariMoviments(ps.getProducte(),'R', ps.getQuantitat(), "Stock restat "));
				drdy.release();
				System.out.println(" Continuacio del producte: " + p.getNomProducte());
				Thread.sleep(rnd.nextInt((ps.getTempsMin()*1000)+(ps.getTempsMax()*1000)));//Tiempo que tarda en fabricar sus mierdas
			} else {
				p.getWait().release();
			}
			
		}//TODO Esta parte no funciona
	}
}