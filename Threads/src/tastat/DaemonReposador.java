package tastat;

import java.util.Date;
import java.util.Random;

public class DaemonReposador extends Thread{

	Magatzem mgz;
	static Random rnd = new Random();
	
	public DaemonReposador(Magatzem m) {
		this.mgz = m;
	}
	
	@Override
	public synchronized void run() {
		try {
			while (true) {		
				if(mgz.getHiHanOrdres() > 0) {
					System.out.println("<---- Daemon Reposador ---->");
					System.out.println("Ordre en proces!");
					OrdreCompra td = mgz.getOrdre();
					Producte p = mgz.getProducte(td.getProducte());
					p.setStock(p.getStock()+td.getQuantitat());
					mgz.getCompres().remove(td);
					mgz.setHiHanOrdres(mgz.getHiHanOrdres()-1);
					mgz.addMoviment(new DiariMoviments(p, 'R', 1, "Reposant", td));
					System.out.println("Ordre preparada!");
					System.out.println("<------------------------->");
					//TODO falta poner lo del punto f
				}
				
				Thread.sleep(500);
			}
		}	
		catch(Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static Comanda getComanda(int id) {
		
		return null;
	}
	
	public static void preararComanda() {
		
	}
}
