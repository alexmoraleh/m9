package tastat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Semaphore;

public class DaemonEscriptor extends Thread {

	// Otra parte de Alex
	// TODO qutar anotaciones de esto es mio esto es tuyo
	// TODO Este parece que tambien esta acabado, habr� que ver.

	Magatzem m;
	Semaphore drdy;

	public DaemonEscriptor(Magatzem m, Semaphore drdy) {
		super();
		this.m = m;
		this.drdy = drdy;
		System.out.println("Escribiendo log...");
		File f = new File("log.txt");
		FileWriter fw;
		BufferedWriter writer;
		try {
			fw = new FileWriter(f,false);
			writer = new BufferedWriter(fw);
			writer.write("Log Tastat Blau. "+ new Date());
			writer.flush();
			writer.close();
			fw.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void run() {
		while (true) {
			BufferedWriter writer;
			try {
				System.out.println("Escribiendo log...");
				File f = new File("log.txt");
				FileWriter fw = new FileWriter(f,true);
				writer = new BufferedWriter(fw);
				drdy.acquire();
				if(m.getMoviments().size() > 0) {
					for (DiariMoviments mov : m.getMoviments()) {
						if(!mov.getInlog()) {
							writer.write(mov.toString(true)+"\n");
						}
					}
				}
				drdy.release();
				writer.flush();
				writer.close();
				fw.close();
				System.out.println("Log escrit!");
				Thread.sleep(30 * 1000);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}
}
