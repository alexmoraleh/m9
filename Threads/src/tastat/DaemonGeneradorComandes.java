package tastat;

import java.util.ArrayDeque;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;

public class DaemonGeneradorComandes extends Thread {
	int ordre = 0;
	private Magatzem mgz;
	Comanda c = null;
	protected List<ComandaLinia> linies;
	protected Semaphore crdy;

	DaemonGeneradorComandes(Magatzem mgz, Semaphore crdy) {
		this.mgz = mgz;
		this.crdy = crdy;
	}

	@Override
	public void run() {
		try {
			
			Producte pliv = null, pllim = null, peri = null, pvel = null;
			for (Producte p : mgz.getProductes()) {
				if (p.getNomProducte().equals("pLiviano"))
					pliv = p;
				if (p.getNomProducte().equals("pLlimona"))
					pllim = p;
				if (p.getNomProducte().equals("pErizo"))
					peri = p;
				if (p.getNomProducte().equals("pVelvet"))
					pvel = p;
			}
						
			while (true) {
				int nClient = (int) (Math.random() * mgz.getClients().size());
				Client cli = mgz.getClients().get(nClient);

						Comanda m1 = new Comanda(cli);
						m1.getLinies().add(new ComandaLinia(pliv, (int) (Math.random() * 40)));
						m1.getLinies().add(new ComandaLinia(pllim, (int) (Math.random() * 30)));
						m1.getLinies().add(new ComandaLinia(peri, (int) (Math.random() * 20)));
						m1.getLinies().add(new ComandaLinia(pvel, (int) (Math.random() * 30)));
						crdy.acquire();
						System.out.println("--------------------------------------------------------");
						System.out.println("--------------- Daemon Generador Comandes --------------");
						System.out.println("--Comanda generada! ID Comanda: "+ m1.getIdComanda() + ".--"); 
						System.out.println("--------------------------------------------------------");
						System.out.println("--------------------------------------------------------");
						mgz.getComandes().add(m1);
						crdy.release();
				Thread.sleep(10000);
			}
		}	
		catch(Exception e) {
			e.printStackTrace();
		};
	}

}
