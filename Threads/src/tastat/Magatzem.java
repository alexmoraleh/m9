package tastat;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class Magatzem {
	
	private List<Producte> magatzem = new ArrayList <Producte>();
	private List<Client>  clients = new ArrayList <Client> ();
	private List<Comanda> comandes = new ArrayList <Comanda>();
    private List<Proveidor> proveidors = new ArrayList <Proveidor>(); 
    private List<Operari> operaris = new ArrayList <Operari>(); 
    private List<DiariMoviments> moviments = new ArrayList <DiariMoviments>();
    private List<OrdreCompra> compres = new ArrayList<OrdreCompra>();
	private List<Object> daemons = new ArrayList<Object>();
	private List<Comanda> comandesp = new ArrayList<Comanda>();
	private int hiHanOrdres = 0; 
	
    public int getHiHanOrdres() {
		return hiHanOrdres;
	}
    
    public OrdreCompra getOrdre() {
		return this.getCompres().get(0);
	}
	
    
    
	public void setHiHanOrdres(int hiHanOrdres) {
		this.hiHanOrdres = hiHanOrdres;
	}

	public List<Comanda> getComandesp() {
		return comandesp;
	}

	public void setComandesp(List<Comanda> comandesp) {
		this.comandesp = comandesp;
	}

	public List<Object> getDaemons() {
		return daemons;
	}

	public void setDaemons(List<Object> daemons) {
		this.daemons = daemons;
	}

	Magatzem(){
	}
	
	Magatzem(List<Producte> lp, List<Client> lc, List<Comanda> lm, List<Proveidor> lpv, List<Operari> lop, List<DiariMoviments> ldm, List<OrdreCompra>loc){
		magatzem = lp;
		clients = lc;
		comandes = lm;
		proveidors = lpv;
		operaris = lop;
		moviments = ldm;
		compres = loc;
	}
	
	public Producte getProducte(Producte tp) {
		for(Producte p:this.getMagatzem()) {
			if(p.equals(tp)) {
				return p;
			}
		}
		return null;
	}
	
	public List<Producte> getMagatzem() {
		return magatzem;
	}

	public void setMagatzem(List<Producte> magatzem) {
		this.magatzem = magatzem;
	}

	public List<DiariMoviments> getMoviments() {
		return moviments;
	}

	public void setMoviments(List<DiariMoviments> moviments) {
		this.moviments = moviments;
	}

	public List<OrdreCompra> getCompres() {
		return compres;
	}

	public void setCompres(List<OrdreCompra> compres) {
		this.compres = compres;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public void setComandes(List<Comanda> comandes) {
		this.comandes = comandes;
	}

	public void setProveidors(List<Proveidor> proveidors) {
		this.proveidors = proveidors;
	}

	public void setOperaris(List<Operari> operaris) {
		this.operaris = operaris;
	}

	boolean afegirQuantitatProducte(int codiProducte, int quantitat){
		boolean trobat = false;
		for (Producte p: magatzem) {
			if (p.getCodiProducte() == codiProducte) {
				p.setStock(p.getStock() + quantitat);
				trobat = true;
				break;
			}
		}
		return trobat;
	}
	
	boolean afegirProducte(Producte prod) {
		boolean trobat = false;
		for (Producte p: magatzem) {
			if (p.getCodiProducte() == prod.getCodiProducte()) {
				trobat = true;
				break;
			}
		}
		if (trobat) 
			return false;
		else {
			magatzem.add(prod);
			return true;
		}
	}
	
	public List <Producte>getProductes(){
		return magatzem;
	}
	
	public List <Client> getClients(){
		return clients;
	}
	
	public List<Comanda> getComandes(){
		return comandes;
	}
	
	public List<Proveidor> getProveidors(){
		return proveidors;
	}
	
	public List<Operari> getOperaris(){
		return operaris;
	}
	
	public Deque<LotDesglossat> apilarCaducats () {
		Iterator<LotDesglossat> it = null;		
		Deque<LotDesglossat> pila = new ArrayDeque<LotDesglossat>();
		LotDesglossat ldg;
		Date avui = new Date();
		for (Producte p: magatzem) {
			it = p.lots.iterator();
			while (it.hasNext()) {
				ldg = it.next();
				if(ldg.getDataCaducitat().compareTo(avui)<0) {
					pila.push((ldg));
					it.remove();
				}
			}
		}
		return pila;
	}
		
	public void addMoviment(DiariMoviments m) {
		moviments.add(m);
	}
	
	
	public boolean add(Producte p) {
		magatzem.add(p);
		return true;
	}
	
	@Override
	public String toString() {
		String s = "";
		
		for (Producte p : magatzem)
			s +=  "\n" + p;
		s += "\nTotal " + magatzem.size() + " Referčncies";
		return s;
	}
	
	
	public Comanda getComanda() {
		for(int i = 0; i < this.getComandes().size(); i++) {
			if(this.getComandes().get(i).getEstat() == ComandaEstat.PENDENT) {
				return this.getComandes().get(i);
			}
		}
		return null;
	}
	
	public void veureComandes() {
		System.out.println("--------------Comandes PENDENTS -----------------");
		System.out.println("-------------------------------------------------");
		for(Comanda c :this.getComandes()) {
			System.out.println(c);
		}
	}
	
	public void veureComandesPreparades() {
		System.out.println("--------------Comandes PREPARADES o en TRANSPORT -----------------");
		System.out.println("------------------------------------------------------------------");
		for(Comanda c :this.getComandesp()) {
			System.out.println(c);
		}
	}
}
