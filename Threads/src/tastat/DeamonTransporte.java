package tastat;

import java.util.concurrent.Semaphore;

public class DeamonTransporte extends Thread {
	
	//Este es mio equis q
	
	Magatzem m;
	int period;
	Semaphore cprep;
	//TODO Creo que Transporte ya esta.
	
	
	public DeamonTransporte(Magatzem m, int period, Semaphore cprep) {
		super();
		this.m = m;
		this.period = period*1000;
		this.cprep = cprep;
	}
	public Magatzem getM() {
		return m;
	}
	public void setM(Magatzem m) {
		this.m = m;
	}
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
	@Override
	public void run() {
		while(true) {//Pumped up kicks
			try {
				int cont=0;
				cprep.acquire();
				for(Comanda c : m.getComandesp()) {
					if(c.getEstat() == ComandaEstat.PREPARADA) {
						System.out.println("<--- Daemon Transport ---->");
						System.out.println("La comanda "+ c.getIdComanda() + " esta en transport.");
						c.setEstat(ComandaEstat.TRANSPORT);
						m.addMoviment(new DiariMoviments('T', 1, "Transportant comanda", c));
						//m.addMoviment(new DiariMoviments(null,-1,'S',1,"Salida correcta",c,null));
						cont++;
						if(cont>=3)break;
					}
				}
				cprep.release();
				Thread.sleep(period);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}

	
}