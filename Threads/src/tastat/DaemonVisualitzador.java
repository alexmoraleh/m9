package tastat;

import java.util.concurrent.Semaphore;

public class DaemonVisualitzador extends Thread {
	int ordre = 0;
	Comanda c = null;
	Magatzem mgz;
	Semaphore crdy;
	Semaphore cprep;
	
	DaemonVisualitzador(Magatzem mgz, Semaphore crdy, Semaphore cprep){
		this.mgz = mgz;
		this.crdy = crdy;
		this.cprep = cprep;
	}
	@Override
	public void run() {
		try {
			while (true) {
				//TODO Se ha modificado las listas de las coamndas ahora hay una lista de pendientes y otra para todos los demas estados de esta forma es mas facil llevar la cuenta.
				crdy.acquire();
				mgz.veureComandes();
				crdy.release();
				cprep.acquire();
				mgz.veureComandesPreparades();
				cprep.release();
				sleep(10000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
