package tastat;

import java.util.concurrent.Semaphore;

public class DeamonGeneradorOperaris extends Thread {

	private Magatzem m;
	private Semaphore crdy;
	private Semaphore prdy;
	private Semaphore cprep;
	private Semaphore drdy;
	private Semaphore ordy;
	private Semaphore enespera;
	private int nOp;

	public DeamonGeneradorOperaris(Magatzem m, Semaphore crdy, Semaphore prdy, Semaphore cprep, Semaphore drdy, Semaphore ordy, Semaphore enespera, int nOp) {
		this.m = m;
		this.crdy = crdy;
		this.prdy = prdy;
		this.cprep = cprep;
		this.drdy = drdy;
		this.ordy = ordy;
		this.enespera = enespera;
		this.nOp = nOp;
	}

	@Override
	public void run() {
		while (true) {
			try {
				if (m.getOperaris().size() < nOp) {
					System.out.println("<---- Daemon Gen Operaris ---->");
					System.out.println("He contractat un nou operari.");
					System.out.println("<----------------------------->");
					Operari o = new Operari(m, crdy, prdy, cprep, drdy, ordy, enespera);
					
					m.getOperaris().add(o);
					o.start();
					
				}
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
