package tastat;

import java.util.Random;
import java.util.concurrent.Semaphore;

@SuppressWarnings("unused")
public class DaemonMiquel extends Thread{
	Magatzem m;
	static int cont = 0;
	static Random rnd = new Random();
	
	public DaemonMiquel(Magatzem m) {
		this.m = m;
	}

	@Override
	public void run() {
		try {			
			while (true) {
				if(cont == 5) {
				     cont = 0;
					 if(m.getOperaris().size() != 0) {
						int ban = (int) Math.random() * m.getOperaris().size();
						System.out.println("------------------------");
						System.out.println("-----Daemon Miquel------");
						System.out.println("L'operari "+ m.getOperaris().get(ban).getIdOperari() + " ha sigut expulsat.");
						System.out.println("------------------------");
						m.addMoviment(new DiariMoviments('E',"Operari "+m.getOperaris().get(ban).getIdOperari()+" expulsat"));
						m.getOperaris().get(ban).banned = true;
						m.getOperaris().remove(ban);
					}
				} else {
					cont++;
				}
				Thread.sleep(60000);
			}
		}	
		catch(Exception e) {
			e.printStackTrace();
		};
	}
}