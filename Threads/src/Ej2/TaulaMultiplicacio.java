package Ej2;

public class TaulaMultiplicacio implements Runnable{
	private int number;

	public TaulaMultiplicacio(int number) {
		this.number=number;
	}
	
	public void run() {
		System.out.printf("Hola soc el thread multiplicador \"%s\" i aquesta es la taula de multiplicar del %d:\n",Thread.currentThread().getName(),number);
		for (int i=1; i<=10; i++){
			System.out.printf("		%d * %d = %d\n",number,i,i*number);
		}
	}

}
