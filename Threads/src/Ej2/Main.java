package Ej2;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner_stdin = new Scanner(System.in);

		System.out.print("Hola, soc la funcio main i el meu nom es \""+Thread.currentThread().getName()+ "\", quin nom vols posar-me? ");
		String nom = scanner_stdin.next( );
		Thread.currentThread().setName(nom);

		System.out.printf("Hola un altre cop, ara em dic \""+Thread.currentThread().getName()+ "\". Quina taula de multiplicar vols que calculi el thread \"taulaMultiplicacio\"?   ");
		int i = scanner_stdin.nextInt();

		
		TaulaMultiplicacio calculadora = new TaulaMultiplicacio(i);
		Thread threadMultiplicador = new Thread(calculadora);

		System.out.printf("Com vols que es digui el thread \"taulaMultiplicacio\"?   ");
		nom = scanner_stdin.next( );
		scanner_stdin.close();
		
		threadMultiplicador.setName(nom);
		threadMultiplicador.start();
	}

}
