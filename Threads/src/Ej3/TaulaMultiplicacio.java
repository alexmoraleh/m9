package Ej3;

public class TaulaMultiplicacio implements Runnable {
	private int number; 
	private int espera; 

	public TaulaMultiplicacio(int number, int espera) { //constructor 
		this.number=number; 
		this.espera=espera; 
    } 

	public	void run() { 
        System.out.printf("Hola soc el thread \"%s\" i aquesta es la taula de multiplicar del %d:\n",Thread.currentThread().getName(),number); 
        for (int i=1; i<=10; i++){ 
        	System.out.printf("%s-->%d * %d = %d\n",Thread.currentThread().getName(),number,i,i*number); 
        	try { 
        			Thread.sleep(espera*1000); 
        	} 
        	catch (InterruptedException e) { 
        		e.printStackTrace(); 
        	} 
        }
	} 

}
