package threads;

import java.util.Random;

import la_factoria.Client;
import la_factoria.Comanda;
import la_factoria.Magatzem;

public class DaemonMiquel extends Thread{
	Magatzem m;
	static int cont = 0;
	static Random rnd = new Random();
	public DaemonMiquel(Magatzem m) {
		this.m = m;
	}

	@Override
	public void run() {
		try {			
			while (true) {
				if(cont != 5) {
					m.getOperaris().remove(rnd.nextInt(0)+m.getOperaris().size());
				} else {
					cont++;
				}
				Thread.sleep(20000);
			}
		}	
		catch(Exception e) {};
	}
}
