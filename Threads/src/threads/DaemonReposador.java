package threads;

import java.util.Date;
import java.util.Random;

import la_factoria.Comanda;
import la_factoria.DiariMoviments;
import la_factoria.Magatzem;
import la_factoria.OrdreCompra;
import la_factoria.Producte;

public class DaemonReposador extends Thread{

	Magatzem mgz;
	static Random rnd = new Random();
	
	@Override
	public void run() {
		try {			
			while (true) {
				Date data = mgz.getCompres().get(0).getDataOrdre();
				OrdreCompra td = mgz.getCompres().get(0);
				for(int i = 0; i < mgz.getCompres().size(); i++) {
					if(data.compareTo(mgz.getCompres().get(i).getDataOrdre()) < 0) {
						data = mgz.getCompres().get(i).getDataOrdre();
						td = mgz.getCompres().get(i);
					}
				}
				
				td.getProducte();
				td.getProveidor();
				td.getQuantitat();
				
				for(Producte p:mgz.getProductes()) {
					if(td.getProducte().getCodiProducte() == p.getCodiProducte()) {
						p.setStock(p.getStock()+td.getQuantitat());
					}
				}
				mgz.getCompres().remove(td);
				mgz.getMoviments().add(new DiariMoviments(td.getProducte(), 1 , 'S', td.getQuantitat(), "Ordre compra ", new Comanda() , td));
				//TODO falta poner lo del punto f
				Thread.sleep(10000);
			}
		}	
		catch(Exception e) {};
	}
	
	public static Comanda getComanda(int id) {
		
		return null;
	}
	
	public static void preararComanda() {
		
	}
}
