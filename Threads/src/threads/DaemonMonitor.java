package threads;

import la_factoria.*;

public class DaemonMonitor implements Runnable{
	
//	Parte Alex :D
	
	Magatzem m;
	
	

	public DaemonMonitor(Magatzem m) {
	super();
	this.m = m;
}



	@Override
	public void run() {
		while(true) {
			try {
				System.out.println("Comandas:");
				for(Comanda c:m.getComandes()) {
					switch (c.getEstat()) {
					case PREPARADA:
						System.out.println("Comanda "+c.getIdComanda()+" preparada par el envio.");
						System.out.println();
						break;
					case TRANSPORT:
						System.out.println("Comanda "+c.getIdComanda()+" en camino al cliente.");
						System.out.println();
						break;
					case PENDENT:
						System.out.println("Comanda "+c.getIdComanda()+" pendiente de fabricación.");
						for(ComandaLinia l:c.getLinies()) {
							System.out.println("Producto "+l.getProducte()+": Cantidad demandada: "+l.getQuantitat()+", cantidad preparada: "+l.getQuantitatPreparada()+". Queda "+(l.getQuantitat()-l.getQuantitatPreparada()));
						}
						break;
					default:
						System.out.println("No c");
						break;
					}
				}
				
				wait(3000);//Se supone que esto espera un momentito para que de tiempo a leer algo
				
				System.out.println();
				System.out.println("Operarios:");
				for(Operari o:m.getOperaris()) {
					System.out.println(o.getIdOperari()+", "+o.getNomOperari()+" pasteles producidos: "+o.getNumPastissos());
				}
				
				wait(1000);
				
				System.out.println();
				for(OrdreCompra o:m.getCompres()) {
					if(o.getEstat()==OrdreEstat.PENDENT) System.out.println("Producto "+o.getProducte()+": Cantidad demandada: "+o.getQuantitat());
				}
				Thread.sleep(30*1000);
			}
			catch (InterruptedException e) {
				// Catch para el wait, creo que es el mismo que para el sleep
				e.printStackTrace();
			}
		}
	}

}
