package threads;

import java.util.List;
import java.util.Random;

import la_factoria.*;

public class DaemonGeneradorComandes extends Thread{
	int ordre = 0;
	private Magatzem mgz;
	Comanda c = null;
	protected List<ComandaLinia> linies;
	static Random rnd = new Random();
	
	public DaemonGeneradorComandes(Magatzem mgz) {
		this.mgz = mgz;
	}

	@Override
	public void run() {
		try {			
			while (true) {
				int nClient = (int) (Math.random() * mgz.getClients().size());
				Client cli = mgz.getClients().get(nClient);
						Comanda m1 = generarComandes(rnd.nextInt(0)+3);
						m1.setClient(cli);
						mgz.getComandes().add(m1);
				Thread.sleep(20000);
			}
		}	
		catch(Exception e) {};
	}

	private static Comanda generarComandes(int ordre) {
		Comanda c = new Comanda();
		switch (ordre) {
		case 0:
			Producte pliv = new Producte("pLiviano", UnitatMesura.UNITAT, 0, null, 1500);
			Producte pllim = new Producte("pLlimona", UnitatMesura.UNITAT, 0, null, 1000);
			Producte peri = new Producte("pErizo", UnitatMesura.UNITAT, 0, null, 1000);
			Producte pvel = new Producte("pVelvet", UnitatMesura.UNITAT, 0, null, 1400);
			ComandaLinia c1 = new ComandaLinia(pliv, 5);
			ComandaLinia c2 = new ComandaLinia(pllim, 10);
			ComandaLinia c3 = new ComandaLinia(peri, 15);
			ComandaLinia c4 = new ComandaLinia(pvel, 20);
			c.getLinies().add(c1);
			c.getLinies().add(c2);
			c.getLinies().add(c3);
			c.getLinies().add(c4);
			return c;
		case 1:
			Producte pliv1 = new Producte("pLiviano", UnitatMesura.UNITAT, 0, null, 1500);
			Producte pllim1 = new Producte("pLlimona", UnitatMesura.UNITAT, 0, null, 1000);
			Producte peri1 = new Producte("pErizo", UnitatMesura.UNITAT, 0, null, 1000);
			Producte pvel1 = new Producte("pVelvet", UnitatMesura.UNITAT, 0, null, 1400);
			ComandaLinia c5 = new ComandaLinia(pliv1, 5);
			ComandaLinia c6 = new ComandaLinia(pllim1, 10);
			ComandaLinia c7 = new ComandaLinia(peri1, 15);
			ComandaLinia c8 = new ComandaLinia(pvel1, 20);
			c.getLinies().add(c5);
			c.getLinies().add(c6);
			c.getLinies().add(c7);
			c.getLinies().add(c8);
			return c;

		case 2:
			Producte pliv2 = new Producte("pLiviano", UnitatMesura.UNITAT, 0, null, 1500);
			Producte pllim2 = new Producte("pLlimona", UnitatMesura.UNITAT, 0, null, 1000);
			Producte peri2 = new Producte("pErizo", UnitatMesura.UNITAT, 0, null, 1000);
			Producte pvel2 = new Producte("pVelvet", UnitatMesura.UNITAT, 0, null, 1400);
			ComandaLinia c9 = new ComandaLinia(pliv2, 5);
			ComandaLinia c10 = new ComandaLinia(pllim2, 10);
			ComandaLinia c11 = new ComandaLinia(peri2, 15);
			ComandaLinia c12 = new ComandaLinia(pvel2, 20);
			c.getLinies().add(c9);
			c.getLinies().add(c10);
			c.getLinies().add(c11);
			c.getLinies().add(c12);
			return c;

		case 3:
			Producte pliv3 = new Producte("pLiviano", UnitatMesura.UNITAT, 0, null, 1500);
			Producte pllim3 = new Producte("pLlimona", UnitatMesura.UNITAT, 0, null, 1000);
			Producte peri3 = new Producte("pErizo", UnitatMesura.UNITAT, 0, null, 1000);
			Producte pvel3 = new Producte("pVelvet", UnitatMesura.UNITAT, 0, null, 1400);
			ComandaLinia c13 = new ComandaLinia(pliv3, 5);
			ComandaLinia c14 = new ComandaLinia(pllim3, 10);
			ComandaLinia c15 = new ComandaLinia(peri3, 15);
			ComandaLinia c16 = new ComandaLinia(pvel3, 20);
			c.getLinies().add(c13);
			c.getLinies().add(c14);
			c.getLinies().add(c15);
			c.getLinies().add(c16);
			return c;

		}
		return null;
	}

}
