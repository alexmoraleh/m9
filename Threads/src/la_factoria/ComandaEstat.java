package la_factoria;

public enum ComandaEstat {
	PENDENT, PREPARADA, TRANSPORT, LLIURADA
}
