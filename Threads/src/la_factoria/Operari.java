package la_factoria;

import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

public class Operari extends Thread{
	
	protected int idOperari;
	protected String nomOperari;
	protected int numPastissos;
	protected Magatzem mgz;
	private Random rnd=new Random();
	
	Operari(){
		idOperari = Generador.getNextOperari();
		Thread.currentThread().setName(Integer.toString(idOperari));
		numPastissos = 0;
	}
	
	
	Operari (Magatzem mgz){
		this();
		this.mgz = mgz;
	}
	
	public int getNumPastissos() {
		return numPastissos;
	}

	public void setNumPastissos(int numPastissos) {
		this.numPastissos = numPastissos;
	}

	Operari(String nom){
		this();
		nomOperari = nom;
	}

	@Override
	public String toString() {
		return (idOperari + ": " + getNomOperari());
	}

	public int getIdOperari() {
		return idOperari;
	}

	public String getNomOperari() {
		return nomOperari;
	}

	public void setNomOperari(String nomOperari) {
		this.nomOperari = nomOperari;
	}
	
	public void run() {
		Producte p;
		for (;;) {
			System.out.println("S�c " + getIdOperari());//TODO Sospecho que esta linea no hace falta
			System.out.println(mgz);
			try {
				for (;;) {
					for (Comanda cm : mgz.getComandes()) {
						if (cm.getEstat()==ComandaEstat.PENDENT) {
							p = cm.rebaixarUnitat();
							//Thread.sleep(2000);//Tiempo fabricando      No se si esto lo voy a usar
							if(p != null) {
								opTrabajando(p);
								
								cm.unitatOK(p);
							}
							
							if(p == null) {
								System.out.println("JA ESTA PREPARADA LA COMANDA "+ cm.getIdComanda());
								cm.setEstat(ComandaEstat.PREPARADA);	
							}
						}
					}
					Thread.sleep(5000);//Cada 60s se duermen 5
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			};
			
		}
				
	}
	private synchronized void opTrabajando(Producte p) throws InterruptedException {
		for(ProducteFabricacio ps:p.getFabricacio()) {
			ArrayList<Producte> ary = new ArrayList<Producte>();
			ary.addAll(mgz.getProductes());//Esto resta stock
			ary.get(ary.indexOf(ps.getProducte())).restStock();//TODO hola futuro yo o futuro tu, igual esto esta mal porque me he motivado un poco, salu2
			System.out.println("fabricando");
			Thread.sleep(rnd.nextInt((ps.getTempsMin()*1000)+(ps.getTempsMax()*1000)));//Tiempo que tarda en fabricar sus mierdas
		}
	}
	/*
	public void run() {
		Producte p;
		for (;;) {
			System.out.println("S�c " + getIdOperari());//TODO Sospecho que esta linea no hace falta
			try {
				for (;;) {
					for (Comanda cm: mgz.getComandes()) {
						if (cm.getEstat()==ComandaEstat.PENDENT) {
							p = cm.rebaixarUnitat();
							if(p != null) {
								List<ProducteFabricacio> lp = p.getFabricacio();
								for(ProducteFabricacio pf:lp) {
									Producte newP = new Producte();
									Producte oldP = new Producte();
									int s = 0;
									for(Producte pt:mgz.getProductes()) {
										if(pt.equals(pf.getProducte())) {
											newP = pt;
											oldP = pt;
											s = pf.getTempsMax();
											newP.setStock(pt.getStock() - pf.getQuantitat());
										}
									}
									Thread.sleep(rnd.nextInt(0)+s*1000);
									mgz.getProductes().remove(oldP);
									mgz.add(newP);
								}
								cm.unitatOK(p);
								
							}
							
							if(p == null) {
								cm.setEstat(ComandaEstat.PREPARADA);
							}
						}
					}
					Thread.sleep(5000);//Cada 60s se duermen 5
				}
			}
			catch(Exception e) {};
			
		}
				
	}
	*/
	
	
}