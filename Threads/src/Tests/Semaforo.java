package Tests;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class Semaforo {

	public static void main(String[] args) {//https://frontal.institutsabadell.cat/cicles-moodle/pluginfile.php/10903/mod_resource/content/8/Apunts%20Bloc%20I%20-%20Processos%20i%20fils%20v26.11.13.pdf
											//Lo dejo en la pagina 36, voy a empezar con la practica.
		Runnable runner=new Runnable() {

			final Random rnd=new Random();
			final Semaphore elttsem=new Semaphore(3);
			int count=0;
			
			@Override
			public void run() {
			int time=rnd.nextInt(15);
			int num=count++;
			try {
				elttsem.acquire();
					System.out.println("Ejecutando la accion n "+num+" durante "+time+"s");
					Thread.sleep(time*2000);
					System.out.println("Ya se finaliz� la accion n "+num);
				elttsem.release();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			}
			
		};
		for (int i=0;i<10;i++) {
			new Thread(runner).start();
		}
	}

}
