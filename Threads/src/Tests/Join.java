package Tests;

public class Join {
	
	public static class JoinRunnable implements Runnable{

		@Override
		public void run() {
			Thread thread=Thread.currentThread();
			System.out.println("Runnable esta siendo arrancado por "+thread.getName());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Turno de "+thread.getName());
		}
		
	}

	public static void main(String[] args) throws InterruptedException {
		JoinRunnable ojo=new JoinRunnable();
		
		Thread thread1=new Thread(ojo, "Hijo 1");
		Thread thread2=new Thread(ojo, "Hijo 2");
		Thread thread3=new Thread(ojo, "Hijo 3");
		Thread thread4=new Thread(ojo, "Hijo 4");
		
		thread1.start();
		thread1.join();
		thread2.start();
		thread2.join();
		thread3.start();
		thread3.join();
		thread4.start();
		thread4.join();
		
		Thread thread5=new Thread(ojo, "Hijo 5");
		Thread thread6=new Thread(ojo, "Hijo 6");
		Thread thread7=new Thread(ojo, "Hijo 7");
		Thread thread8=new Thread(ojo, "Hijo 8");
		
		thread5.start();
		thread6.start();
		thread7.start();
		thread8.start();
	}

}
