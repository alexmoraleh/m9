package Tests;

public class Yield {
	public static class FilFora implements Runnable{
		String str;
		
		public FilFora (String str) {
			this.str=str;
		}
		@Override
		public void run() {
			if(str=="A") {
				while(true) {
					System.out.println(str);
				}
			}
			else {
				while(true) {
					System.out.println(str);
					Thread.yield();
				}
			}
		}
		
	}

	public static void main(String[] args) {
		new Thread(new FilFora("A")).start();
		new Thread(new FilFora("B")).start();
	}

}
