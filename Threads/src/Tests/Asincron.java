package Tests;

import java.util.Random;

public class Asincron extends Thread{
	Random rnd=new Random();
	@SuppressWarnings("deprecation")
	public void run() {
		for(int i=1;i<10;i++) {
			System.out.println("Accion concurrente: "+this.getName());
			try {
				if(this.getName().equals("Hijo 1")) Thread.sleep(10000);
				else Thread.sleep((long) (Math.random()*1000));
				//System.out.println(this.getName()+" dormido");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			/*if(rnd.nextBoolean()) {
				stop();
			}*/
		}
		
	}

	public static void main(String[] args) {
		Thread a1=new Thread(new Asincron());
		Thread a2=new Thread(new Asincron());
		
		a1.setName("Hijo 1");
		a2.setName("Hijo 2");
		
		a1.start();
		a2.start();
		System.out.println("Fin del programa");
	}

}
