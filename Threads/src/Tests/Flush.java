package Tests;

public class Flush {
	public static class PingPong extends Thread{
		String str;
		
		public PingPong (String str) {
			this.str=str;
		}
		@Override
		public void run() {
			for(int i=0;i<300;i++) {
				System.out.println(str);
				System.out.flush();
			}
		}
		
	}

	public static void main(String[] args) {
		Thread Ping=new PingPong("Ping");
		Thread Pong=new PingPong("Pong");
		
		Pong.setPriority(Thread.MAX_PRIORITY);
		Ping.setPriority(Thread.MIN_PRIORITY);
		
		Ping.start();
		Pong.start();
		
	}

}
