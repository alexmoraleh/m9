package Ej1;

import java.util.Scanner;

public class Threat{
	static Scanner rd=new Scanner(System.in);
	public static void main(String[] args) {
		Par par=new Par();
		Imp imp=new Imp();
		
		Thread threadpar=new Thread(par);
		threadpar.setName("threadPar");
		Thread threadimp=new Thread(imp);
		threadimp.setName("threadImp");
		threadpar.start();
		threadimp.start();
		rd.nextInt();
		//Thread
	}
}
class Par extends Thread{
	
	public void run() {
		for(int i=0;i<=1000000;i=i+2) {
			System.out.println("Pares "+i);
		}
	}
	
}
class Imp extends Thread{
	public void run() {
		for(int i=1;i<1000000;i=i+2) {
			System.out.println("Impares "+i);
		}
	}
}
